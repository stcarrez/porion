Version 0.16.0 - Under development
  - Run the build process with a pseudo TTY to solve output order issues
  - Fix handling the recipe name in identification strings for some commands

Version 0.15.0 - Jul 2024
  - Improvement of build environment configuration setup
  - Preliminary support for per-project secret keys

Version 0.14.0 - Apr 2023
  - Fix adding a recipe on a new branch
  - Fix displaying the project recipes on all branches
  - Use single ssh connection for multiple commands when building on remote node
  - Copy project files using scp on remote nodes

Version 0.13.0 - Mar 2023
  - Update to use last AWA version
  - Improvement to display on mobile phones
  - Support to collect standard error as separate streams to identify
    messages as errors
  - Prototype for build templates

Version 0.12.0 - Oct 2022
  - Add support for notification
  - Add global activity dashboard
  - Use Frappe Charts instead of jQuery Flot (remove jQuery)

Version 0.11.0 - Sep 2022
  - Update for AWA development version (2.5.0)
  - Add support to user login with AWA

Version 0.10.0 - Aug 2022
  - Update presentation of build results in the Web server
  - Update collecting test results from a remote host through ssh
  - Update to use AWA 2.4.0
  - Add graph in project view to show the evolution of unit tests over the builds
  - Display the build nodes with their builds
  - Display the project and build unit tests
  - Display the recipes queued for a build

Version 0.9.0  - Jul 2022
  - Fix cleaning of build directories when it contains socket files

Version 0.8.0  - Jun 2022
  - Update to latest Ada Web Application
  - Update the set command to allow changing the source URL and source scheme

Version 0.7.0   - Feb 2022
  - Added Debian packages for porion and porion-server
  - Added list of recipes in the project page
  - Added man page for porion-server

Version 0.6.0   - Jan 2022
  - REST API to get a schields badge using https://shields.io/endpoint
  - Build dynamo and are if they are not installed
  - Setup for first Debian package

Version 0.5.0   - Dec 2021
  - Server with Web responsive UI listing the projects and builds
  - REST API to get the build logs
  - Minimal operations to install the porion agent after a build
  - Minimal man page and documentation

