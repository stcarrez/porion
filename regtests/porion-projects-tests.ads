-----------------------------------------------------------------------
--  porion-projects-tests -- Tests for projects package
--  Copyright (C) 2021, 2023 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------

with Util.Tests;
with Porion.Testsuite;
package Porion.Projects.Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Porion.Testsuite.Test with null record;

   --  Test parsing a project identification string.
   procedure Test_Parse (T : in out Test);

   --  Test parsing invalid project identification string.
   procedure Test_Parse_Error (T : in out Test);

   --  Test converting the Ident_Type back to a string.
   procedure Test_To_String (T : in out Test);

end Porion.Projects.Tests;
