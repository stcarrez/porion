-----------------------------------------------------------------------
--  porion-templates-tests -- Unit tests for porion templates
--  Copyright (C) 2022, 2023 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------

with Util.Assertions;
with Util.Test_Caller;

with Porion.Configs;
with Porion.Executors;
with Porion.Sources;
with Porion.Templates.Parser;
with Porion.Services;
with Porion.Testsuite;
with Porion.Projects.Services;
with Porion.Templates.Generator;
with Porion.Builds.Services;
package body Porion.Templates.Tests is

   package Caller is new Util.Test_Caller (Test, "Porion.Templates");

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Porion.Templates.Parser.Load",
                       Test_Parse_Template'Access);
      Caller.Add_Test (Suite, "Test Porion.Templates.Parser.Load (Error)",
                       Test_Parse_Error'Access);
      Caller.Add_Test (Suite, "Test Porion.Templates.Generator.Generate",
                       Test_Generate'Access);
   end Add_Tests;

   --  ------------------------------
   --  Test parsing several valid templates.
   --  ------------------------------
   procedure Test_Parse_Template (T : in out Test) is
      Path       : constant String := Util.Tests.Get_Test_Path ("tmpl-1");
      Service    : Porion.Services.Context_Type;
      Tmpl_Path  : constant String := Util.Tests.Get_Path ("regtests/files/templates");
      Tmpl       : Porion.Templates.Template_Ref;
   begin
      Porion.Testsuite.Create (Service, Path, True);
      Porion.Templates.Set_Search_Paths (Service.Templates, Tmpl_Path);
      Parser.Load (Tmpl_Path & "/gnu.porion", Service.Templates);
      Parser.Load (Tmpl_Path & "/local.porion", Service.Templates);
      Parser.Load (Tmpl_Path & "/ado.porion", Service.Templates, Tmpl);
   end Test_Parse_Template;

   --  ------------------------------
   --  Test parsing several valid templates.
   --  ------------------------------
   procedure Test_Parse_Error (T : in out Test) is
      Path       : constant String := Util.Tests.Get_Path ("regtests/files/bad-templates");
      Repository : Porion.Templates.Repository_Type;
      Tmpl       : Porion.Templates.Template_Ref;
   begin
      Parser.Load (Path & "/bad-rule-1.porion", Repository, Tmpl);
      T.Assert (Tmpl.Is_Null, "Load returned a template");
   end Test_Parse_Error;

   --  ------------------------------
   --  Test the generation of project recipes from a template.
   --  ------------------------------
   procedure Test_Generate (T : in out Test) is
      Dir       : constant String := Util.Tests.Get_Test_Path ("tmpls");
      Tmpl_Path : constant String := Util.Tests.Get_Path ("regtests/files/templates");
      Service   : Porion.Services.Context_Type;
      Build     : Porion.Builds.Services.Build_Config_Vector;
      Tmpl      : Porion.Templates.Template_Ref;
      Info      : Porion.Sources.Source_Info_Type;
      Branch    : Porion.Sources.Branch_Type;
   begin
      Porion.Testsuite.Create (Service, Dir, True);
      Porion.Testsuite.Add_Build_Node (Service, "freebsd");
      Porion.Testsuite.Add_Build_Node (Service, "netbsd");
      Porion.Testsuite.Add_Build_Node (Service, "dionysos");
      Porion.Testsuite.Add_Build_Node (Service, "boree");
      Porion.Templates.Set_Search_Paths (Service.Templates, Tmpl_Path);
      Porion.Templates.Parser.Load (Tmpl_Path & "/local.porion", Service.Templates, Tmpl);

      begin
         Service.Load_Project ("hello");

      exception
         when Porion.Services.Not_Found =>
            Info.Kind := Porion.SRC_FILE;
            Info.Name := To_UString ("hello");
            Info.URI := To_UString ("regtests/files/hello-1.0.tar.gz");
            Projects.Services.Add_Project (Service, Info);
      end;
      Projects.Services.Add_Branch (Service, "master", Branch, True);
      Porion.Templates.Generator.Generate (Service, Tmpl, Build);
      Porion.Builds.Services.Update (Service, Build);
   end Test_Generate;

end Porion.Templates.Tests;
