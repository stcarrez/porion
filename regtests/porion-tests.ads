-----------------------------------------------------------------------
--  porion-commands-tests -- Tests for porion commands
--  Copyright (C) 2021, 2022 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------

with Util.Tests;
with Porion.Testsuite;
package Porion.Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Porion.Testsuite.Test with null record;

   procedure Test_Init (T : in out Test);

   procedure Test_Add (T : in out Test);

   --  Test registration of a project, build and scan of some fake unit tests.
   procedure Test_Add_With_Tests (T : in out Test);

   --  Test registration of a project, adding recipes and scan of some fake unit tests.
   procedure Test_Add_Recipe_With_Tests (T : in out Test);

   --  Test registration of a project, adding recipes, building and removing a recipe with builds.
   procedure Test_Remove_Recipe_With_Tests (T : in out Test);

end Porion.Tests;
