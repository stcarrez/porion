pragma synchronous=OFF;
/* Copied from porion_server-drop-sqlite.sql*/
/* File generated automatically by dynamo */
/* Copied from porion_lib-drop-sqlite.sql*/
/* File generated automatically by dynamo */
DROP TABLE IF EXISTS `porion_test_run_group`;
DROP TABLE IF EXISTS `porion_test_run`;
DROP TABLE IF EXISTS `porion_test_group`;
DROP TABLE IF EXISTS `porion_test`;
DROP TABLE IF EXISTS `porion_project`;
DROP TABLE IF EXISTS `porion_dependency`;
DROP TABLE IF EXISTS `porion_branch`;
DROP TABLE IF EXISTS `porion_node`;
DROP TABLE IF EXISTS `porion_metric`;
DROP TABLE IF EXISTS `porion_build_metric`;
DROP TABLE IF EXISTS `porion_recipe_step`;
DROP TABLE IF EXISTS `porion_recipe`;
DROP TABLE IF EXISTS `porion_last_build`;
DROP TABLE IF EXISTS `porion_environment`;
DROP TABLE IF EXISTS `porion_build_step`;
DROP TABLE IF EXISTS `porion_build_reason`;
DROP TABLE IF EXISTS `porion_build_queue`;
DROP TABLE IF EXISTS `porion_build_executor`;
DROP TABLE IF EXISTS `porion_build`;
/* Copied from awa-drop-sqlite.sql*/
/* File generated automatically by dynamo */
DROP TABLE IF EXISTS `awa_user`;
DROP TABLE IF EXISTS `awa_session`;
DROP TABLE IF EXISTS `awa_email`;
DROP TABLE IF EXISTS `awa_access_key`;
DROP TABLE IF EXISTS `awa_permission`;
DROP TABLE IF EXISTS `awa_acl`;
DROP TABLE IF EXISTS `awa_oauth_session`;
DROP TABLE IF EXISTS `awa_callback`;
DROP TABLE IF EXISTS `awa_application`;
DROP TABLE IF EXISTS `awa_queue`;
DROP TABLE IF EXISTS `awa_message_type`;
DROP TABLE IF EXISTS `awa_message`;
DROP TABLE IF EXISTS `awa_audit_field`;
DROP TABLE IF EXISTS `awa_audit`;
/* Copied from ado-drop-sqlite.sql*/
/* File generated automatically by dynamo */
DROP TABLE IF EXISTS `sequence`;
DROP TABLE IF EXISTS `entity_type`;
