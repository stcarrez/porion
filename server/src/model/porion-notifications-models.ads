-----------------------------------------------------------------------
--  Porion.Notifications.Models -- Porion.Notifications.Models
-----------------------------------------------------------------------
--  File generated by Dynamo DO NOT MODIFY
--  Template used: templates/model/package-spec.xhtml
--  Ada Generator: https://github.com/stcarrez/dynamo Version 1.4.0
-----------------------------------------------------------------------
--  Copyright (C) 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------
pragma Warnings (Off);
with ADO.Sessions;
with ADO.Objects;
with ADO.Statements;
with ADO.SQL;
with ADO.Schemas;
with ADO.Queries;
with ADO.Queries.Loaders;
with Ada.Calendar;
with Ada.Containers.Vectors;
with Ada.Strings.Unbounded;
with Util.Beans.Objects;
with Util.Beans.Objects.Enums;
with Util.Beans.Basic.Lists;
with ADO.Audits;
with AWA.Users.Models;
with Porion.Builds.Models;
with Porion.Projects.Models;
pragma Warnings (On);
package Porion.Notifications.Models is

   pragma Style_Checks ("-mrIu");

   type Frequency_Type is (NOTIFY_IMMEDIATE, NOTIFY_DAILY, NOTIFY_DISABLED, NOTIFY_WEEKLY);
   for Frequency_Type use (NOTIFY_IMMEDIATE => 0, NOTIFY_DAILY => 1, NOTIFY_DISABLED => 2, NOTIFY_WEEKLY => 3);
   package Frequency_Type_Objects is
      new Util.Beans.Objects.Enums (Frequency_Type);

   type Nullable_Frequency_Type is record
      Is_Null : Boolean := True;
      Value   : Frequency_Type;
   end record;

   type Subscriber_Ref is new ADO.Objects.Object_Ref with null record;

   --  --------------------
   --  The subscriber table indicates a user that is interested to be notified by changes
   --  on the project.
   --  --------------------
   --  Create an object key for Subscriber.
   function Subscriber_Key (Id : in ADO.Identifier) return ADO.Objects.Object_Key;
   --  Create an object key for Subscriber from a string.
   --  Raises Constraint_Error if the string cannot be converted into the object key.
   function Subscriber_Key (Id : in String) return ADO.Objects.Object_Key;

   Null_Subscriber : constant Subscriber_Ref;
   function "=" (Left, Right : Subscriber_Ref'Class) return Boolean;

   --  Set the subscriber id
   procedure Set_Id (Object : in out Subscriber_Ref;
                     Value  : in ADO.Identifier);

   --  Get the subscriber id
   function Get_Id (Object : in Subscriber_Ref)
                 return ADO.Identifier;
   --  Get the optimistic lock version
   function Get_Version (Object : in Subscriber_Ref)
                 return Integer;

   --  Set the date and time of the last notification.
   procedure Set_Last_Notification_Date (Object : in out Subscriber_Ref;
                                         Value  : in ADO.Nullable_Time);

   --  Get the date and time of the last notification.
   function Get_Last_Notification_Date (Object : in Subscriber_Ref)
                 return ADO.Nullable_Time;

   --  Set the date and time of the last check for notification.
   procedure Set_Check_Date (Object : in out Subscriber_Ref;
                             Value  : in ADO.Nullable_Time);

   --  Get the date and time of the last check for notification.
   function Get_Check_Date (Object : in Subscriber_Ref)
                 return ADO.Nullable_Time;

   --
   procedure Set_Frequency (Object : in out Subscriber_Ref;
                            Value  : in Frequency_Type);

   --
   function Get_Frequency (Object : in Subscriber_Ref)
                 return Frequency_Type;

   --
   procedure Set_Send_Count (Object : in out Subscriber_Ref;
                             Value  : in Integer);

   --
   function Get_Send_Count (Object : in Subscriber_Ref)
                 return Integer;

   --  Set notify when some configuration is changed.
   procedure Set_Notify_Config (Object : in out Subscriber_Ref;
                                Value  : in Boolean);

   --  Get notify when some configuration is changed.
   function Get_Notify_Config (Object : in Subscriber_Ref)
                 return Boolean;

   --  Set notify when a build is made
   procedure Set_Notify_Build (Object : in out Subscriber_Ref;
                               Value  : in Boolean);

   --  Get notify when a build is made
   function Get_Notify_Build (Object : in Subscriber_Ref)
                 return Boolean;

   --  Set notify when unit tests change
   procedure Set_Notify_Xunit (Object : in out Subscriber_Ref;
                               Value  : in Boolean);

   --  Get notify when unit tests change
   function Get_Notify_Xunit (Object : in Subscriber_Ref)
                 return Boolean;

   --  Set the last build that was reported
   procedure Set_Last_Reported_Build (Object : in out Subscriber_Ref;
                                      Value  : in Porion.Builds.Models.Build_Ref'Class);

   --  Get the last build that was reported
   function Get_Last_Reported_Build (Object : in Subscriber_Ref)
                 return Porion.Builds.Models.Build_Ref'Class;

   --
   procedure Set_Project (Object : in out Subscriber_Ref;
                          Value  : in Porion.Projects.Models.Project_Ref'Class);

   --
   function Get_Project (Object : in Subscriber_Ref)
                 return Porion.Projects.Models.Project_Ref'Class;

   --  Set the last audit field that was reported.
   procedure Set_Last_Change (Object : in out Subscriber_Ref;
                              Value  : in ADO.Identifier);

   --  Get the last audit field that was reported.
   function Get_Last_Change (Object : in Subscriber_Ref)
                 return ADO.Identifier;

   --
   procedure Set_Email (Object : in out Subscriber_Ref;
                        Value  : in AWA.Users.Models.Email_Ref'Class);

   --
   function Get_Email (Object : in Subscriber_Ref)
                 return AWA.Users.Models.Email_Ref'Class;

   --  Load the entity identified by 'Id'.
   --  Raises the NOT_FOUND exception if it does not exist.
   procedure Load (Object  : in out Subscriber_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Id      : in ADO.Identifier);

   --  Load the entity identified by 'Id'.
   --  Returns True in <b>Found</b> if the object was found and False if it does not exist.
   procedure Load (Object  : in out Subscriber_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Id      : in ADO.Identifier;
                   Found   : out Boolean);

   --  Reload from the database the same object if it was modified.
   --  Returns True in `Updated` if the object was reloaded.
   --  Raises the NOT_FOUND exception if it does not exist.
   procedure Reload (Object  : in out Subscriber_Ref;
                     Session : in out ADO.Sessions.Session'Class;
                     Updated : out Boolean);

   --  Find and load the entity.
   overriding
   procedure Find (Object  : in out Subscriber_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Query   : in ADO.SQL.Query'Class;
                   Found   : out Boolean);

   --  Save the entity.  If the entity does not have an identifier, an identifier is allocated
   --  and it is inserted in the table.  Otherwise, only data fields which have been changed
   --  are updated.
   overriding
   procedure Save (Object  : in out Subscriber_Ref;
                   Session : in out ADO.Sessions.Master_Session'Class);

   --  Delete the entity.
   overriding
   procedure Delete (Object  : in out Subscriber_Ref;
                     Session : in out ADO.Sessions.Master_Session'Class);

   overriding
   function Get_Value (From : in Subscriber_Ref;
                       Name : in String) return Util.Beans.Objects.Object;

   --  Table definition
   SUBSCRIBER_TABLE : constant ADO.Schemas.Class_Mapping_Access;

   --  Internal method to allocate the Object_Record instance
   overriding
   procedure Allocate (Object : in out Subscriber_Ref);

   --  Copy of the object.
   procedure Copy (Object : in Subscriber_Ref;
                   Into   : in out Subscriber_Ref);

   package Subscriber_Vectors is
      new Ada.Containers.Vectors (Index_Type   => Positive,
                                  Element_Type => Subscriber_Ref,
                                  "="          => "=");
   subtype Subscriber_Vector is Subscriber_Vectors.Vector;

   procedure List (Object  : in out Subscriber_Vector;
                   Session : in out ADO.Sessions.Session'Class;
                   Query   : in ADO.SQL.Query'Class);

   type Audit_Info is record

      --  the audit id.
      Audit_Id : ADO.Identifier;

      --  the date when the change was made.
      Date : Ada.Calendar.Time;

      --  the field name that was changed.
      Kind : ADO.Entity_Type;

      --  the field name that was changed.
      Field : Ada.Strings.Unbounded.Unbounded_String;

      --  the id that was changed.
      Id : ADO.Identifier;

      --  the old value.
      Old_Value : ADO.Nullable_String;

      --  the new value.
      New_Value : ADO.Nullable_String;
   end record;

   package Audit_Info_Vectors is
      new Ada.Containers.Vectors (Index_Type   => Positive,
                                  Element_Type => Audit_Info,
                                  "="          => "=");

   subtype Audit_Info_Vector is Audit_Info_Vectors.Vector;

   --  Run the query controlled by <b>Context</b> and append the list in <b>Object</b>.
   procedure List (Object  : in out Audit_Info_Vector;
                   Session : in out ADO.Sessions.Session'Class;
                   Context : in out ADO.Queries.Context'Class);

   Query_Audit_List : constant ADO.Queries.Query_Definition_Access;

   --  --------------------
   --    Information about a notification.
   --  --------------------
   type Notification_Info is
     new Util.Beans.Basic.Bean with  record

      --  the subscriber id.
      Id : ADO.Identifier;

      --  the project name.
      Name : Ada.Strings.Unbounded.Unbounded_String;

      --  the project status.
      Project_Status : Porion.Projects.Models.Project_Status_Type;

      --  the user email address.
      Email : Ada.Strings.Unbounded.Unbounded_String;

      --  the notification frequency.
      Frequency : Frequency_Type;

      --  the last notification date.
      Last_Notification_Date : ADO.Nullable_Time;

      --  the last check date.
      Check_Date : ADO.Nullable_Time;

      --  the number of notification sent.
      Send_Count : Natural;

      --  the notify config flag.
      Notify_Config : Boolean;

      --  the notify build flag.
      Notify_Build : Boolean;

      --  the notify xunit flag.
      Notify_Xunit : Boolean;
   end record;

   --  Get the bean attribute identified by the name.
   overriding
   function Get_Value (From : in Notification_Info;
                       Name : in String) return Util.Beans.Objects.Object;

   --  Set the bean attribute identified by the name.
   overriding
   procedure Set_Value (Item  : in out Notification_Info;
                        Name  : in String;
                        Value : in Util.Beans.Objects.Object);


   package Notification_Info_Beans is
      new Util.Beans.Basic.Lists (Element_Type => Notification_Info);
   package Notification_Info_Vectors renames Notification_Info_Beans.Vectors;
   subtype Notification_Info_List_Bean is Notification_Info_Beans.List_Bean;

   type Notification_Info_List_Bean_Access is access all Notification_Info_List_Bean;

   --  Run the query controlled by <b>Context</b> and append the list in <b>Object</b>.
   procedure List (Object  : in out Notification_Info_List_Bean'Class;
                   Session : in out ADO.Sessions.Session'Class;
                   Context : in out ADO.Queries.Context'Class);

   subtype Notification_Info_Vector is Notification_Info_Vectors.Vector;

   --  Run the query controlled by <b>Context</b> and append the list in <b>Object</b>.
   procedure List (Object  : in out Notification_Info_Vector;
                   Session : in out ADO.Sessions.Session'Class;
                   Context : in out ADO.Queries.Context'Class);

   Query_Notification_List : constant ADO.Queries.Query_Definition_Access;



private
   SUBSCRIBER_NAME : aliased constant String := "porion_subscriber";
   COL_0_1_NAME : aliased constant String := "id";
   COL_1_1_NAME : aliased constant String := "version";
   COL_2_1_NAME : aliased constant String := "last_notification_date";
   COL_3_1_NAME : aliased constant String := "check_date";
   COL_4_1_NAME : aliased constant String := "frequency";
   COL_5_1_NAME : aliased constant String := "send_count";
   COL_6_1_NAME : aliased constant String := "notify_config";
   COL_7_1_NAME : aliased constant String := "notify_build";
   COL_8_1_NAME : aliased constant String := "notify_xunit";
   COL_9_1_NAME : aliased constant String := "last_reported_build_id";
   COL_10_1_NAME : aliased constant String := "project_id";
   COL_11_1_NAME : aliased constant String := "last_change";
   COL_12_1_NAME : aliased constant String := "email_id";

   SUBSCRIBER_DEF : aliased constant ADO.Schemas.Class_Mapping :=
     (Count   => 13,
      Table   => SUBSCRIBER_NAME'Access,
      Members => (
         1 => COL_0_1_NAME'Access,
         2 => COL_1_1_NAME'Access,
         3 => COL_2_1_NAME'Access,
         4 => COL_3_1_NAME'Access,
         5 => COL_4_1_NAME'Access,
         6 => COL_5_1_NAME'Access,
         7 => COL_6_1_NAME'Access,
         8 => COL_7_1_NAME'Access,
         9 => COL_8_1_NAME'Access,
         10 => COL_9_1_NAME'Access,
         11 => COL_10_1_NAME'Access,
         12 => COL_11_1_NAME'Access,
         13 => COL_12_1_NAME'Access)
     );
   SUBSCRIBER_TABLE : constant ADO.Schemas.Class_Mapping_Access
      := SUBSCRIBER_DEF'Access;

   SUBSCRIBER_AUDIT_DEF : aliased constant ADO.Audits.Auditable_Mapping :=
     (Count    => 4,
      Of_Class => SUBSCRIBER_DEF'Access,
      Members  => (
         1 => 4,
         2 => 6,
         3 => 7,
         4 => 8)
     );
   SUBSCRIBER_AUDIT_TABLE : constant ADO.Audits.Auditable_Mapping_Access
      := SUBSCRIBER_AUDIT_DEF'Access;

   Null_Subscriber : constant Subscriber_Ref
      := Subscriber_Ref'(ADO.Objects.Object_Ref with null record);

   type Subscriber_Impl is
      new ADO.Audits.Auditable_Object_Record (Key_Type => ADO.Objects.KEY_INTEGER,
                                     Of_Class => SUBSCRIBER_DEF'Access,
                                     With_Audit => SUBSCRIBER_AUDIT_DEF'Access)
   with record
       Version : Integer;
       Last_Notification_Date : ADO.Nullable_Time;
       Check_Date : ADO.Nullable_Time;
       Frequency : Frequency_Type;
       Send_Count : Integer;
       Notify_Config : Boolean;
       Notify_Build : Boolean;
       Notify_Xunit : Boolean;
       Last_Reported_Build : Porion.Builds.Models.Build_Ref;
       Project : Porion.Projects.Models.Project_Ref;
       Last_Change : ADO.Identifier;
       Email : AWA.Users.Models.Email_Ref;
   end record;

   type Subscriber_Access is access all Subscriber_Impl;

   overriding
   procedure Destroy (Object : access Subscriber_Impl);

   overriding
   procedure Find (Object  : in out Subscriber_Impl;
                   Session : in out ADO.Sessions.Session'Class;
                   Query   : in ADO.SQL.Query'Class;
                   Found   : out Boolean);

   overriding
   procedure Load (Object  : in out Subscriber_Impl;
                   Session : in out ADO.Sessions.Session'Class);
   procedure Load (Object  : in out Subscriber_Impl;
                   Stmt    : in out ADO.Statements.Query_Statement'Class;
                   Session : in out ADO.Sessions.Session'Class);

   overriding
   procedure Save (Object  : in out Subscriber_Impl;
                   Session : in out ADO.Sessions.Master_Session'Class);

   overriding
   procedure Create (Object  : in out Subscriber_Impl;
                     Session : in out ADO.Sessions.Master_Session'Class);

   overriding
   procedure Delete (Object  : in out Subscriber_Impl;
                     Session : in out ADO.Sessions.Master_Session'Class);

   procedure Set_Field (Object : in out Subscriber_Ref'Class;
                        Impl   : out Subscriber_Access);

   package File_1 is
      new ADO.Queries.Loaders.File (Path => "notifications-changes.xml",
                                    Sha1 => "22791F462B305268BDD92E8BFC0C39CE16A66300");

   package Def_Auditinfo_Audit_List is
      new ADO.Queries.Loaders.Query (Name => "audit-list",
                                     File => File_1.File'Access);
   Query_Audit_List : constant ADO.Queries.Query_Definition_Access
   := Def_Auditinfo_Audit_List.Query'Access;

   package File_2 is
      new ADO.Queries.Loaders.File (Path => "notifications.xml",
                                    Sha1 => "1F7ABC2E691C3EEC40CCED9B1777BCD430145106");

   package Def_Notificationinfo_Notification_List is
      new ADO.Queries.Loaders.Query (Name => "notification-list",
                                     File => File_2.File'Access);
   Query_Notification_List : constant ADO.Queries.Query_Definition_Access
   := Def_Notificationinfo_Notification_List.Query'Access;
end Porion.Notifications.Models;
