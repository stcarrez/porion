-----------------------------------------------------------------------
--  porion-notifications-rest -- REST API for the notifications
--  Copyright (C) 2022 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------

with Security.Permissions;
with OpenAPI.Servers;
with OpenAPI.Servers.Operation;
package Porion.Notifications.Rest is

   procedure Post_Subscriber
     (Req     : in out OpenAPI.Servers.Request'Class;
      Reply   : in out OpenAPI.Servers.Response'Class;
      Stream  : in out OpenAPI.Servers.Output_Stream'Class;
      Context : in out OpenAPI.Servers.Context_Type);

   package API_Post_Subscriber is
     new OpenAPI.Servers.Operation
        (Handler => Post_Subscriber,
         Method  => OpenAPI.Servers.POST,
         Permission => Security.Permissions.NONE,
         URI     => "/api/v1/subscribers",
         Mimes   => null);

end Porion.Notifications.Rest;
