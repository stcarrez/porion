-----------------------------------------------------------------------
--  Porion.Nodes.Models -- Porion.Nodes.Models
-----------------------------------------------------------------------
--  File generated by Dynamo DO NOT MODIFY
--  Template used: templates/model/package-body.xhtml
--  Ada Generator: https://github.com/stcarrez/dynamo Version 1.4.0
-----------------------------------------------------------------------
--  Copyright (C) 2024 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------
pragma Warnings (Off);
with Ada.Unchecked_Deallocation;
with Util.Beans.Objects.Time;
pragma Warnings (On);
package body Porion.Nodes.Models is

   pragma Style_Checks ("-mrIu");
   pragma Warnings (Off, "formal parameter * is not referenced");
   pragma Warnings (Off, "use clause for type *");
   pragma Warnings (Off, "use clause for private type *");

   use type ADO.Objects.Object_Record_Access;
   use type ADO.Objects.Object_Ref;

   function Node_Key (Id : in ADO.Identifier) return ADO.Objects.Object_Key is
      Result : ADO.Objects.Object_Key (Of_Type  => ADO.Objects.KEY_INTEGER,
                                       Of_Class => NODE_DEF'Access);
   begin
      ADO.Objects.Set_Value (Result, Id);
      return Result;
   end Node_Key;

   function Node_Key (Id : in String) return ADO.Objects.Object_Key is
      Result : ADO.Objects.Object_Key (Of_Type  => ADO.Objects.KEY_INTEGER,
                                       Of_Class => NODE_DEF'Access);
   begin
      ADO.Objects.Set_Value (Result, Id);
      return Result;
   end Node_Key;

   function "=" (Left, Right : Node_Ref'Class) return Boolean is
   begin
      return ADO.Objects.Object_Ref'Class (Left) = ADO.Objects.Object_Ref'Class (Right);
   end "=";

   procedure Set_Field (Object : in out Node_Ref'Class;
                        Impl   : out Node_Access) is
      Result : ADO.Objects.Object_Record_Access;
   begin
      Object.Prepare_Modify (Result);
      Impl := Node_Impl (Result.all)'Access;
   end Set_Field;

   --  Internal method to allocate the Object_Record instance
   overriding
   procedure Allocate (Object : in out Node_Ref) is
      Impl : Node_Access;
   begin
      Impl := new Node_Impl;
      Impl.Version := 0;
      Impl.Create_Date := ADO.DEFAULT_TIME;
      Impl.Update_Date.Is_Null := True;
      Impl.Kind := Porion.Nodes.Node_Type'First;
      Impl.Deleted := False;
      Impl.Status := Porion.Nodes.Node_Status_Type'First;
      Impl.Info_Date.Is_Null := True;
      Impl.Main_Node := False;
      ADO.Objects.Set_Object (Object, Impl.all'Access);
   end Allocate;

   -- ----------------------------------------
   --  Data object: Node
   -- ----------------------------------------

   procedure Set_Id (Object : in out Node_Ref;
                     Value  : in ADO.Identifier) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Key_Value (Impl.all, 1, Value);
   end Set_Id;

   function Get_Id (Object : in Node_Ref)
                  return ADO.Identifier is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Object.all)'Access;
   begin
      return Impl.Get_Key_Value;
   end Get_Id;


   function Get_Version (Object : in Node_Ref)
                  return Integer is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Version;
   end Get_Version;


   procedure Set_Name (Object : in out Node_Ref;
                        Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_String (Impl.all, 3, Impl.Name, Value);
   end Set_Name;

   procedure Set_Name (Object : in out Node_Ref;
                       Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Unbounded_String (Impl.all, 3, Impl.Name, Value);
   end Set_Name;

   function Get_Name (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Name);
   end Get_Name;
   function Get_Name (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Name;
   end Get_Name;


   procedure Set_Login (Object : in out Node_Ref;
                         Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_String (Impl.all, 4, Impl.Login, Value);
   end Set_Login;

   procedure Set_Login (Object : in out Node_Ref;
                        Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Unbounded_String (Impl.all, 4, Impl.Login, Value);
   end Set_Login;

   function Get_Login (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Login);
   end Get_Login;
   function Get_Login (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Login;
   end Get_Login;


   procedure Set_Repository_Dir (Object : in out Node_Ref;
                                  Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_String (Impl.all, 5, Impl.Repository_Dir, Value);
   end Set_Repository_Dir;

   procedure Set_Repository_Dir (Object : in out Node_Ref;
                                 Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Unbounded_String (Impl.all, 5, Impl.Repository_Dir, Value);
   end Set_Repository_Dir;

   function Get_Repository_Dir (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Repository_Dir);
   end Get_Repository_Dir;
   function Get_Repository_Dir (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Repository_Dir;
   end Get_Repository_Dir;


   procedure Set_Parameters (Object : in out Node_Ref;
                              Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_String (Impl.all, 6, Impl.Parameters, Value);
   end Set_Parameters;

   procedure Set_Parameters (Object : in out Node_Ref;
                             Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Unbounded_String (Impl.all, 6, Impl.Parameters, Value);
   end Set_Parameters;

   function Get_Parameters (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Parameters);
   end Get_Parameters;
   function Get_Parameters (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Parameters;
   end Get_Parameters;


   procedure Set_Create_Date (Object : in out Node_Ref;
                              Value  : in Ada.Calendar.Time) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Time (Impl.all, 7, Impl.Create_Date, Value);
   end Set_Create_Date;

   function Get_Create_Date (Object : in Node_Ref)
                  return Ada.Calendar.Time is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Create_Date;
   end Get_Create_Date;


   procedure Set_Update_Date (Object : in out Node_Ref;
                              Value  : in ADO.Nullable_Time) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Time (Impl.all, 8, Impl.Update_Date, Value);
   end Set_Update_Date;

   function Get_Update_Date (Object : in Node_Ref)
                  return ADO.Nullable_Time is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Update_Date;
   end Get_Update_Date;


   procedure Set_Kind (Object : in out Node_Ref;
                       Value  : in Porion.Nodes.Node_Type) is
      procedure Set_Field_Discrete is
        new ADO.Audits.Set_Field_Operation
          (Porion.Nodes.Node_Type,
           Porion.Nodes.Node_Type_Objects.To_Object);
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      Set_Field_Discrete (Impl.all, 9, Impl.Kind, Value);
   end Set_Kind;

   function Get_Kind (Object : in Node_Ref)
                  return Porion.Nodes.Node_Type is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Kind;
   end Get_Kind;


   procedure Set_Deleted (Object : in out Node_Ref;
                          Value  : in Boolean) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Boolean (Impl.all, 10, Impl.Deleted, Value);
   end Set_Deleted;

   function Get_Deleted (Object : in Node_Ref)
                  return Boolean is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Deleted;
   end Get_Deleted;


   procedure Set_Status (Object : in out Node_Ref;
                         Value  : in Porion.Nodes.Node_Status_Type) is
      procedure Set_Field_Discrete is
        new ADO.Objects.Set_Field_Operation
          (Porion.Nodes.Node_Status_Type);
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      Set_Field_Discrete (Impl.all, 11, Impl.Status, Value);
   end Set_Status;

   function Get_Status (Object : in Node_Ref)
                  return Porion.Nodes.Node_Status_Type is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Status;
   end Get_Status;


   procedure Set_Os_Name (Object : in out Node_Ref;
                           Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_String (Impl.all, 12, Impl.Os_Name, Value);
   end Set_Os_Name;

   procedure Set_Os_Name (Object : in out Node_Ref;
                          Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Unbounded_String (Impl.all, 12, Impl.Os_Name, Value);
   end Set_Os_Name;

   function Get_Os_Name (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Os_Name);
   end Get_Os_Name;
   function Get_Os_Name (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Os_Name;
   end Get_Os_Name;


   procedure Set_Os_Version (Object : in out Node_Ref;
                              Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_String (Impl.all, 13, Impl.Os_Version, Value);
   end Set_Os_Version;

   procedure Set_Os_Version (Object : in out Node_Ref;
                             Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Unbounded_String (Impl.all, 13, Impl.Os_Version, Value);
   end Set_Os_Version;

   function Get_Os_Version (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Os_Version);
   end Get_Os_Version;
   function Get_Os_Version (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Os_Version;
   end Get_Os_Version;


   procedure Set_Info_Date (Object : in out Node_Ref;
                            Value  : in ADO.Nullable_Time) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Time (Impl.all, 14, Impl.Info_Date, Value);
   end Set_Info_Date;

   function Get_Info_Date (Object : in Node_Ref)
                  return ADO.Nullable_Time is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Info_Date;
   end Get_Info_Date;


   procedure Set_Cpu_Type (Object : in out Node_Ref;
                            Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_String (Impl.all, 15, Impl.Cpu_Type, Value);
   end Set_Cpu_Type;

   procedure Set_Cpu_Type (Object : in out Node_Ref;
                           Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Objects.Set_Field_Unbounded_String (Impl.all, 15, Impl.Cpu_Type, Value);
   end Set_Cpu_Type;

   function Get_Cpu_Type (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Cpu_Type);
   end Get_Cpu_Type;
   function Get_Cpu_Type (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Cpu_Type;
   end Get_Cpu_Type;


   procedure Set_Main_Node (Object : in out Node_Ref;
                            Value  : in Boolean) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Boolean (Impl.all, 16, Impl.Main_Node, Value);
   end Set_Main_Node;

   function Get_Main_Node (Object : in Node_Ref)
                  return Boolean is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Main_Node;
   end Get_Main_Node;


   procedure Set_Check_Rules (Object : in out Node_Ref;
                               Value : in String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_String (Impl.all, 17, Impl.Check_Rules, Value);
   end Set_Check_Rules;

   procedure Set_Check_Rules (Object : in out Node_Ref;
                              Value  : in Ada.Strings.Unbounded.Unbounded_String) is
      Impl : Node_Access;
   begin
      Set_Field (Object, Impl);
      ADO.Audits.Set_Field_Unbounded_String (Impl.all, 17, Impl.Check_Rules, Value);
   end Set_Check_Rules;

   function Get_Check_Rules (Object : in Node_Ref)
                 return String is
   begin
      return Ada.Strings.Unbounded.To_String (Object.Get_Check_Rules);
   end Get_Check_Rules;
   function Get_Check_Rules (Object : in Node_Ref)
                  return Ada.Strings.Unbounded.Unbounded_String is
      Impl : constant Node_Access
         := Node_Impl (Object.Get_Load_Object.all)'Access;
   begin
      return Impl.Check_Rules;
   end Get_Check_Rules;

   --  Copy of the object.
   procedure Copy (Object : in Node_Ref;
                   Into   : in out Node_Ref) is
      Result : Node_Ref;
   begin
      if not Object.Is_Null then
         declare
            Impl : constant Node_Access
              := Node_Impl (Object.Get_Load_Object.all)'Access;
            Copy : constant Node_Access
              := new Node_Impl;
         begin
            ADO.Objects.Set_Object (Result, Copy.all'Access);
            Copy.Copy (Impl.all);
            Copy.Version := Impl.Version;
            Copy.Name := Impl.Name;
            Copy.Login := Impl.Login;
            Copy.Repository_Dir := Impl.Repository_Dir;
            Copy.Parameters := Impl.Parameters;
            Copy.Create_Date := Impl.Create_Date;
            Copy.Update_Date := Impl.Update_Date;
            Copy.Kind := Impl.Kind;
            Copy.Deleted := Impl.Deleted;
            Copy.Status := Impl.Status;
            Copy.Os_Name := Impl.Os_Name;
            Copy.Os_Version := Impl.Os_Version;
            Copy.Info_Date := Impl.Info_Date;
            Copy.Cpu_Type := Impl.Cpu_Type;
            Copy.Main_Node := Impl.Main_Node;
            Copy.Check_Rules := Impl.Check_Rules;
         end;
      end if;
      Into := Result;
   end Copy;

   overriding
   procedure Find (Object  : in out Node_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Query   : in ADO.SQL.Query'Class;
                   Found   : out Boolean) is
      Impl  : constant Node_Access := new Node_Impl;
   begin
      Impl.Find (Session, Query, Found);
      if Found then
         ADO.Objects.Set_Object (Object, Impl.all'Access);
      else
         ADO.Objects.Set_Object (Object, null);
         Destroy (Impl);
      end if;
   end Find;

   procedure Load (Object  : in out Node_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Id      : in ADO.Identifier) is
      Impl  : constant Node_Access := new Node_Impl;
      Found : Boolean;
      Query : ADO.SQL.Query;
   begin
      Query.Bind_Param (Position => 1, Value => Id);
      Query.Set_Filter ("id = ?");
      Impl.Find (Session, Query, Found);
      if not Found then
         Destroy (Impl);
         raise ADO.Objects.NOT_FOUND;
      end if;
      ADO.Objects.Set_Object (Object, Impl.all'Access);
   end Load;

   procedure Load (Object  : in out Node_Ref;
                   Session : in out ADO.Sessions.Session'Class;
                   Id      : in ADO.Identifier;
                   Found   : out Boolean) is
      Impl  : constant Node_Access := new Node_Impl;
      Query : ADO.SQL.Query;
   begin
      Query.Bind_Param (Position => 1, Value => Id);
      Query.Set_Filter ("id = ?");
      Impl.Find (Session, Query, Found);
      if not Found then
         Destroy (Impl);
      else
         ADO.Objects.Set_Object (Object, Impl.all'Access);
      end if;
   end Load;

   procedure Reload (Object  : in out Node_Ref;
                     Session : in out ADO.Sessions.Session'Class;
                     Updated : out Boolean) is
      Result : ADO.Objects.Object_Record_Access;
      Impl   : Node_Access;
      Query  : ADO.SQL.Query;
      Id     : ADO.Identifier;
   begin
      if Object.Is_Null then
         raise ADO.Objects.NULL_ERROR;
      end if;
      Object.Prepare_Modify (Result);
      Impl := Node_Impl (Result.all)'Access;
      Id := ADO.Objects.Get_Key_Value (Impl.all);
      Query.Bind_Param (Position => 1, Value => Id);
      Query.Bind_Param (Position => 2, Value => Impl.Version);
      Query.Set_Filter ("id = ? AND version != ?");
      declare
         Stmt : ADO.Statements.Query_Statement
             := Session.Create_Statement (Query, NODE_DEF'Access);
      begin
         Stmt.Execute;
         if Stmt.Has_Elements then
            Updated := True;
            Impl.Load (Stmt, Session);
         else
            Updated := False;
         end if;
      end;
   end Reload;

   overriding
   procedure Save (Object  : in out Node_Ref;
                   Session : in out ADO.Sessions.Master_Session'Class) is
      Impl : ADO.Objects.Object_Record_Access := Object.Get_Object;
   begin
      if Impl = null then
         Impl := new Node_Impl;
         ADO.Objects.Set_Object (Object, Impl);
      end if;
      if not ADO.Objects.Is_Created (Impl.all) then
         Impl.Create (Session);
      else
         Impl.Save (Session);
      end if;
   end Save;

   overriding
   procedure Delete (Object  : in out Node_Ref;
                     Session : in out ADO.Sessions.Master_Session'Class) is
      Impl : constant ADO.Objects.Object_Record_Access := Object.Get_Object;
   begin
      if Impl /= null then
         Impl.Delete (Session);
      end if;
   end Delete;

   --  --------------------
   --  Free the object
   --  --------------------
   overriding
   procedure Destroy (Object : access Node_Impl) is
      type Node_Impl_Ptr is access all Node_Impl;
      procedure Unchecked_Free is new Ada.Unchecked_Deallocation
              (Node_Impl, Node_Impl_Ptr);
      pragma Warnings (Off, "*redundant conversion*");
      Ptr : Node_Impl_Ptr := Node_Impl (Object.all)'Access;
      pragma Warnings (On, "*redundant conversion*");
   begin
      Unchecked_Free (Ptr);
   end Destroy;

   overriding
   procedure Find (Object  : in out Node_Impl;
                   Session : in out ADO.Sessions.Session'Class;
                   Query   : in ADO.SQL.Query'Class;
                   Found   : out Boolean) is
      Stmt : ADO.Statements.Query_Statement
          := Session.Create_Statement (Query, NODE_DEF'Access);
   begin
      Stmt.Execute;
      if Stmt.Has_Elements then
         Object.Load (Stmt, Session);
         Stmt.Next;
         Found := not Stmt.Has_Elements;
      else
         Found := False;
      end if;
   end Find;

   overriding
   procedure Load (Object  : in out Node_Impl;
                   Session : in out ADO.Sessions.Session'Class) is
      Found : Boolean;
      Query : ADO.SQL.Query;
      Id    : constant ADO.Identifier := Object.Get_Key_Value;
   begin
      Query.Bind_Param (Position => 1, Value => Id);
      Query.Set_Filter ("id = ?");
      Object.Find (Session, Query, Found);
      if not Found then
         raise ADO.Objects.NOT_FOUND;
      end if;
   end Load;

   overriding
   procedure Save (Object  : in out Node_Impl;
                   Session : in out ADO.Sessions.Master_Session'Class) is
      Stmt : ADO.Statements.Update_Statement
         := Session.Create_Statement (NODE_DEF'Access);
   begin
      if Object.Is_Modified (1) then
         Stmt.Save_Field (Name  => COL_0_1_NAME, --  id
                          Value => Object.Get_Key);
         Object.Clear_Modified (1);
      end if;
      if Object.Is_Modified (3) then
         Stmt.Save_Field (Name  => COL_2_1_NAME, --  name
                          Value => Object.Name);
         Object.Clear_Modified (3);
      end if;
      if Object.Is_Modified (4) then
         Stmt.Save_Field (Name  => COL_3_1_NAME, --  login
                          Value => Object.Login);
         Object.Clear_Modified (4);
      end if;
      if Object.Is_Modified (5) then
         Stmt.Save_Field (Name  => COL_4_1_NAME, --  repository_dir
                          Value => Object.Repository_Dir);
         Object.Clear_Modified (5);
      end if;
      if Object.Is_Modified (6) then
         Stmt.Save_Field (Name  => COL_5_1_NAME, --  parameters
                          Value => Object.Parameters);
         Object.Clear_Modified (6);
      end if;
      if Object.Is_Modified (7) then
         Stmt.Save_Field (Name  => COL_6_1_NAME, --  create_date
                          Value => Object.Create_Date);
         Object.Clear_Modified (7);
      end if;
      if Object.Is_Modified (8) then
         Stmt.Save_Field (Name  => COL_7_1_NAME, --  update_date
                          Value => Object.Update_Date);
         Object.Clear_Modified (8);
      end if;
      if Object.Is_Modified (9) then
         Stmt.Save_Field (Name  => COL_8_1_NAME, --  kind
                          Value => Integer (Porion.Nodes.Node_Type'Enum_Rep (Object.Kind)));
         Object.Clear_Modified (9);
      end if;
      if Object.Is_Modified (10) then
         Stmt.Save_Field (Name  => COL_9_1_NAME, --  deleted
                          Value => Object.Deleted);
         Object.Clear_Modified (10);
      end if;
      if Object.Is_Modified (11) then
         Stmt.Save_Field (Name  => COL_10_1_NAME, --  status
                          Value => Integer (Porion.Nodes.Node_Status_Type'Enum_Rep (Object.Status)));
         Object.Clear_Modified (11);
      end if;
      if Object.Is_Modified (12) then
         Stmt.Save_Field (Name  => COL_11_1_NAME, --  os_name
                          Value => Object.Os_Name);
         Object.Clear_Modified (12);
      end if;
      if Object.Is_Modified (13) then
         Stmt.Save_Field (Name  => COL_12_1_NAME, --  os_version
                          Value => Object.Os_Version);
         Object.Clear_Modified (13);
      end if;
      if Object.Is_Modified (14) then
         Stmt.Save_Field (Name  => COL_13_1_NAME, --  info_date
                          Value => Object.Info_Date);
         Object.Clear_Modified (14);
      end if;
      if Object.Is_Modified (15) then
         Stmt.Save_Field (Name  => COL_14_1_NAME, --  cpu_type
                          Value => Object.Cpu_Type);
         Object.Clear_Modified (15);
      end if;
      if Object.Is_Modified (16) then
         Stmt.Save_Field (Name  => COL_15_1_NAME, --  main_node
                          Value => Object.Main_Node);
         Object.Clear_Modified (16);
      end if;
      if Object.Is_Modified (17) then
         Stmt.Save_Field (Name  => COL_16_1_NAME, --  check_rules
                          Value => Object.Check_Rules);
         Object.Clear_Modified (17);
      end if;
      if Stmt.Has_Save_Fields then
         Object.Version := Object.Version + 1;
         Stmt.Save_Field (Name  => "version",
                          Value => Object.Version);
         Stmt.Set_Filter (Filter => "id = ? and version = ?");
         Stmt.Add_Param (Value => Object.Get_Key);
         Stmt.Add_Param (Value => Object.Version - 1);
         declare
            Result : Integer;
         begin
            Stmt.Execute (Result);
            if Result /= 1 then
               if Result /= 0 then
                  raise ADO.Objects.UPDATE_ERROR;
               else
                  raise ADO.Objects.LAZY_LOCK;
               end if;
            end if;
            ADO.Audits.Save (Object, Session);
         end;
      end if;
   end Save;

   overriding
   procedure Create (Object  : in out Node_Impl;
                     Session : in out ADO.Sessions.Master_Session'Class) is
      Query : ADO.Statements.Insert_Statement
                  := Session.Create_Statement (NODE_DEF'Access);
      Result : Integer;
   begin
      Object.Version := 1;
      Session.Allocate (Id => Object);
      Query.Save_Field (Name  => COL_0_1_NAME, --  id
                        Value => Object.Get_Key);
      Query.Save_Field (Name  => COL_1_1_NAME, --  version
                        Value => Object.Version);
      Query.Save_Field (Name  => COL_2_1_NAME, --  name
                        Value => Object.Name);
      Query.Save_Field (Name  => COL_3_1_NAME, --  login
                        Value => Object.Login);
      Query.Save_Field (Name  => COL_4_1_NAME, --  repository_dir
                        Value => Object.Repository_Dir);
      Query.Save_Field (Name  => COL_5_1_NAME, --  parameters
                        Value => Object.Parameters);
      Query.Save_Field (Name  => COL_6_1_NAME, --  create_date
                        Value => Object.Create_Date);
      Query.Save_Field (Name  => COL_7_1_NAME, --  update_date
                        Value => Object.Update_Date);
      Query.Save_Field (Name  => COL_8_1_NAME, --  kind
                        Value => Integer (Porion.Nodes.Node_Type'Enum_Rep (Object.Kind)));
      Query.Save_Field (Name  => COL_9_1_NAME, --  deleted
                        Value => Object.Deleted);
      Query.Save_Field (Name  => COL_10_1_NAME, --  status
                        Value => Integer (Porion.Nodes.Node_Status_Type'Enum_Rep (Object.Status)));
      Query.Save_Field (Name  => COL_11_1_NAME, --  os_name
                        Value => Object.Os_Name);
      Query.Save_Field (Name  => COL_12_1_NAME, --  os_version
                        Value => Object.Os_Version);
      Query.Save_Field (Name  => COL_13_1_NAME, --  info_date
                        Value => Object.Info_Date);
      Query.Save_Field (Name  => COL_14_1_NAME, --  cpu_type
                        Value => Object.Cpu_Type);
      Query.Save_Field (Name  => COL_15_1_NAME, --  main_node
                        Value => Object.Main_Node);
      Query.Save_Field (Name  => COL_16_1_NAME, --  check_rules
                        Value => Object.Check_Rules);
      Query.Execute (Result);
      if Result /= 1 then
         raise ADO.Objects.INSERT_ERROR;
      end if;
      ADO.Objects.Set_Created (Object);
      ADO.Audits.Save (Object, Session);
   end Create;

   overriding
   procedure Delete (Object  : in out Node_Impl;
                     Session : in out ADO.Sessions.Master_Session'Class) is
      Stmt : ADO.Statements.Delete_Statement
         := Session.Create_Statement (NODE_DEF'Access);
   begin
      Stmt.Set_Filter (Filter => "id = ?");
      Stmt.Add_Param (Value => Object.Get_Key);
      Stmt.Execute;
   end Delete;

   --  ------------------------------
   --  Get the bean attribute identified by the name.
   --  ------------------------------
   overriding
   function Get_Value (From : in Node_Ref;
                       Name : in String) return Util.Beans.Objects.Object is
      Obj  : ADO.Objects.Object_Record_Access;
      Impl : access Node_Impl;
   begin
      if From.Is_Null then
         return Util.Beans.Objects.Null_Object;
      end if;
      Obj := From.Get_Load_Object;
      Impl := Node_Impl (Obj.all)'Access;
      if Name = "id" then
         return ADO.Objects.To_Object (Impl.Get_Key);
      elsif Name = "name" then
         return Util.Beans.Objects.To_Object (Impl.Name);
      elsif Name = "login" then
         return Util.Beans.Objects.To_Object (Impl.Login);
      elsif Name = "repository_dir" then
         return Util.Beans.Objects.To_Object (Impl.Repository_Dir);
      elsif Name = "parameters" then
         return Util.Beans.Objects.To_Object (Impl.Parameters);
      elsif Name = "create_date" then
         return Util.Beans.Objects.Time.To_Object (Impl.Create_Date);
      elsif Name = "update_date" then
         if Impl.Update_Date.Is_Null then
            return Util.Beans.Objects.Null_Object;
         else
            return Util.Beans.Objects.Time.To_Object (Impl.Update_Date.Value);
         end if;
      elsif Name = "kind" then
         return Porion.Nodes.Node_Type_Objects.To_Object (Impl.Kind);
      elsif Name = "deleted" then
         return Util.Beans.Objects.To_Object (Impl.Deleted);
      elsif Name = "status" then
         return Porion.Nodes.Node_Status_Type_Objects.To_Object (Impl.Status);
      elsif Name = "os_name" then
         return Util.Beans.Objects.To_Object (Impl.Os_Name);
      elsif Name = "os_version" then
         return Util.Beans.Objects.To_Object (Impl.Os_Version);
      elsif Name = "info_date" then
         if Impl.Info_Date.Is_Null then
            return Util.Beans.Objects.Null_Object;
         else
            return Util.Beans.Objects.Time.To_Object (Impl.Info_Date.Value);
         end if;
      elsif Name = "cpu_type" then
         return Util.Beans.Objects.To_Object (Impl.Cpu_Type);
      elsif Name = "main_node" then
         return Util.Beans.Objects.To_Object (Impl.Main_Node);
      elsif Name = "check_rules" then
         return Util.Beans.Objects.To_Object (Impl.Check_Rules);
      end if;
      return Util.Beans.Objects.Null_Object;
   end Get_Value;



   --  ------------------------------
   --  Load the object from current iterator position
   --  ------------------------------
   procedure Load (Object  : in out Node_Impl;
                   Stmt    : in out ADO.Statements.Query_Statement'Class;
                   Session : in out ADO.Sessions.Session'Class) is
      pragma Unreferenced (Session);
   begin
      Object.Set_Key_Value (Stmt.Get_Identifier (0));
      Object.Name := Stmt.Get_Unbounded_String (2);
      Object.Login := Stmt.Get_Unbounded_String (3);
      Object.Repository_Dir := Stmt.Get_Unbounded_String (4);
      Object.Parameters := Stmt.Get_Unbounded_String (5);
      Object.Create_Date := Stmt.Get_Time (6);
      Object.Update_Date := Stmt.Get_Nullable_Time (7);
      Object.Kind := Porion.Nodes.Node_Type'Enum_Val (Stmt.Get_Integer (8));
      Object.Deleted := Stmt.Get_Boolean (9);
      Object.Status := Porion.Nodes.Node_Status_Type'Enum_Val (Stmt.Get_Integer (10));
      Object.Os_Name := Stmt.Get_Unbounded_String (11);
      Object.Os_Version := Stmt.Get_Unbounded_String (12);
      Object.Info_Date := Stmt.Get_Nullable_Time (13);
      Object.Cpu_Type := Stmt.Get_Unbounded_String (14);
      Object.Main_Node := Stmt.Get_Boolean (15);
      Object.Check_Rules := Stmt.Get_Unbounded_String (16);
      Object.Version := Stmt.Get_Integer (1);
      ADO.Objects.Set_Created (Object);
   end Load;


end Porion.Nodes.Models;
