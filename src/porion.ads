-----------------------------------------------------------------------
--  porion -- Continuous integration controller
--  Copyright (C) 2021, 2022 Stephane Carrez
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
-----------------------------------------------------------------------
with Intl;
with Ada.Strings.Unbounded;
with Util.Beans.Objects.Enums;
package Porion is

   type Source_Control_Type is (SRC_UNKNOWN, SRC_GIT, SRC_FILE);

   --  Use a fixed enum configuration because the value is stored in database.
   for Source_Control_Type use (SRC_UNKNOWN => 0,
                                SRC_GIT => 1,
                                SRC_FILE => 2);

   package Source_Control_Type_Objects is
      new Util.Beans.Objects.Enums (Source_Control_Type);

   type Microsecond_Type is new Natural;

   type Millisecond_Type is new Natural;

   type Permille_Type is new Natural;

   subtype Rate_Type is Permille_Type range 0 .. 100;

   function "-" (Message : in String) return String is (Intl."-" (Message));

   subtype UString is Ada.Strings.Unbounded.Unbounded_String;

   function To_UString (Value : in String) return UString
     renames Ada.Strings.Unbounded.To_Unbounded_String;

   function To_String (Value : in UString) return String
     renames Ada.Strings.Unbounded.To_String;

   function Length (Value : in UString) return Natural
     renames Ada.Strings.Unbounded.Length;

   function "=" (Left, Right : in UString) return Boolean
     renames Ada.Strings.Unbounded."=";
   function "=" (Left : in UString; Right : in String) return Boolean
     renames Ada.Strings.Unbounded."=";

private

   --  Configure the logs.
   procedure Configure_Logs (Debug   : in Boolean;
                             Dump    : in Boolean;
                             Verbose : in Boolean);

   function To_Filter_Rules (List : in String) return String;

end Porion;
